import React, { Component } from "react";
import Board from "./board";
class Game extends Component {
  state = {
    boards: [
      { squares: Array(9).fill(null), xIsNext: true, winner: null, index: 0 },
    ],
    currentPointingBoard: 0,
  };
  render() {
    console.log(this.state.boards);
    const currentBoard = this.state.boards[this.state.currentPointingBoard];
    let status;
    if (currentBoard.winner) {
      status = "Winner is player : " + currentBoard.winner;
    } else {
      status = "Next player: " + (currentBoard.xIsNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board squares={currentBoard.squares} onClick={this.handleClick} />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>
            {this.state.boards.map((board) => (
              <li>
                {" "}
                <button onClick={() => this.handleHistoryTravel(board.index)}>
                  {"Step:" + board.index}{" "}
                </button>
              </li>
            ))}
          </ol>
        </div>
      </div>
    );
  }

  handleClick = (i) => {
    const currentBoard = {
      ...this.state.boards[this.state.currentPointingBoard],
    };
    const squares = [...currentBoard.squares];

    if (squares[i] !== null || currentBoard.winner) {
      return;
    }
    squares[i] = currentBoard.xIsNext ? "X" : "O";
    const xIsNext = !currentBoard.xIsNext;
    const winner = this.calculateWinner(squares);
    const index = currentBoard.index + 1;
    const newBoard = { squares, xIsNext, winner, index };
    const currentBoards = this.state.boards.slice(
      0,
      this.state.currentPointingBoard + 1
    );
    const boards = [...currentBoards, newBoard];
    const currentPointingBoard = this.state.currentPointingBoard + 1;
    this.setState({ boards, currentPointingBoard });
  };

  calculateWinner = (squares) => {
    const winnerArray = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let i = 0; i < winnerArray.length; i++) {
      const [a, b, c] = winnerArray[i];
      if (squares[a] && squares[a] === squares[b] && squares[b] === squares[c])
        return squares[a];
    }

    return null;
  };

  handleHistoryTravel = (i) => {
    this.setState({ currentPointingBoard: i });
  };
}

export default Game;
